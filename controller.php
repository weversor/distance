<?php
include_once("./models/city.php");
include_once("./models/distance.php");
include_once("./services/DistanceService.php");
include_once("./services/CityService.php");
include_once("./services/DBConnector.php");

header('Content-Type: application/json; charset=utf-8');

if (isset($_POST['request']) && $_POST['request'] == 'get_cities')
{
    print \services\CityService::getInstance()->getAllCities();
    exit;
}

if (isset($_POST['request']) && $_POST['request'] == 'get_distances')
{
    foreach ($_POST['cities'] as $city)
    {
        $city = new \models\City($city['id'], $city['name']);
        \services\DistanceService::getInstance()->add($city);
    }
    print \services\DistanceService::getInstance()->getAllDistanceAsJSON();
    exit;
}
