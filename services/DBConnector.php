<?php
namespace services;


class DBConnector
{
    private static $_instance = null;
    public $pdo;

    private function __construct()
    {
        $CONFIG = parse_ini_file("./config.ini");
        $this->pdo = new \PDO('mysql:host=' . $CONFIG['host'] . ';dbname=' . $CONFIG['dbname'], $CONFIG['username'], $CONFIG['userpass']);
    }

    public static function getInstance()
    {
        if(is_null(self::$_instance))
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
}