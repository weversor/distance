<?php
namespace services;

use models\City;

class DistanceService
{
    private $cities = [];
    private static $_instance = null;
    private $db;

    private function __construct()
    {
        $this->db = DBConnector::getInstance();
    }

    public static function getInstance()
    {
        if(is_null(self::$_instance))
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function add(City $city)
    {
        $this->cities[] = $city;
    }

    public function getDistance(City $cityFrom, City $cityTo)
    {
        $sql = 'SELECT *
                     FROM `distance`
                          WHERE id_from < :idFrom AND id_to = :idTo LIMIT 1';

        $sth = $this->db->pdo->prepare($sql);
        $sth->execute(array(':idFrom' => $cityFrom->getId(), ':idTo' => $cityTo->getId()));
        $result  = $sth->fetch();
        return new \Distance($result['id_from'], $result['id_to'], $result['distance']);
    }

    public function getAllDistanceAsJSON()
    {
        $arResult = [];
        $summ = 0;
        for($index = 0; $index < sizeof($this->cities) - 1; $index++)
        {
            $cityFrom = $this->cities[$index];
            $cityTo = $this->cities[$index+1];
            try {
                if (!($cityFrom instanceof City && $cityTo instanceof City))
                    throw new \Exception('Incorrect Data');
            }
            catch (\Exception $e)
            {
                die(json_encode([
                    'status' => 'error',
                    'message' => $e->getMessage()
                ]));
            }
            $distance = $this->getDistance($cityFrom, $cityTo);
            $arResult[] = [
                'from' => $cityFrom->getName(),
                'to' => $cityTo->getName(),
                'distance' => $distance->getDistance()
            ];
            $summ += $distance->getDistance();
        }
        $arResult['summ'] = $summ;
        return json_encode($arResult, JSON_UNESCAPED_UNICODE);
    }
}