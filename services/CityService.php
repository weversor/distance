<?php
namespace services;

class CityService
{
    private static $_instance = null;
    private $db;

    private function __construct()
    {
        $this->db = DBConnector::getInstance();
    }

    public static function getInstance()
    {
        if(is_null(self::$_instance))
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function getAllCities()
    {
        $sql = 'SELECT *
                     FROM `city`';

        $sth = $this->db->pdo->query($sql);
        $result  = $sth->fetchAll();
        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}