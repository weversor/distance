<?php
class Distance 
{
	private $idFrom;
	private $idTo;
	private $distance;
	
	public function __construct($idFrom, $idTo, $distance)
	{
		$this->idFrom = $idFrom;
		$this->idTo = $idTo;
		$this->distance = $distance;
	}	
	
	public function setIdFrom($idFrom) 
	{
		$this->idFrom = $idFrom;
	}
	
	public function setIdTo($idTo) 
	{
		$this->idTo = $idTo;
	}
	
	public function setDistance($distance) 
	{
		$this->distance = $distance;
	}
	
	public function getIdFrom() 
	{
		return $this->idFrom;
	}
	
	public function getIdTo() 
	{
		return $this->idTo;
	}
	
	public function getDistance() 
	{
		return $this->distance;
	}
}
