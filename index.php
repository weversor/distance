<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ru">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<title>Calc distances</title>
    <script src="/public/js/jquery-2.1.1.min.js"></script>
</head>
<body class="docs ">
	<div>
        <select id="cities">
            <option disabled selected>Оберіть місто</option>
        </select>
    </div>
    <div>
        <h2>Обрані міста</h2>
        <ul id="cities-list">
        </ul>
    </div>
    <div>
        <button id="calc-button">Розрахувати відстань</button>
    </div>
    <div>
        <h2>Результат</h2>
        <textarea rows="20" cols="100" id="result-body"></textarea>
    </div>
    <script>
        $(document).ready(function(){
            var cities = [];
            $.post('/controller.php', {'request' : 'get_cities'}, function(data) {
                for (i in data) {
                    $("#cities").append($("<option name='" + data[i].id + "'>" + data[i].name + "</option>"));
                }
            });
            $("#cities ").change(function () {
                if (cities.length >=5) return false;
                $( "select option:selected" ).each(function() {
                    var city = {
                        'id': $(this).attr('name'),
                        'name': $(this).text()
                    };
                    cities.push(city);
                    $("#cities-list").append($("<li>" + city.name + "</li>"))
                });
            });
            $("#calc-button").click(function(){
                if (cities.length < 2) return false;
                $.post('/controller.php', {'request' : 'get_distances', 'cities': cities}, function(data) {
                    console.log(data);
                    $("#result-body").val(JSON.stringify(data));
                });
                return false;
            });
        });
    </script>
</body>
</html>
